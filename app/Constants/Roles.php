<?php

namespace App\Constants;

class Roles
{
    public const SUPERADMIN = 'SUPER_ADMIN';
    public const ADMIN      = 'ADMIN';
    public const OPERATOR   = 'OPERATOR';

    public const SUPERADMIN_ID = 1;
    public const ADMIN_ID      = 2;
    public const OPERATOR_ID   = 3;
}
