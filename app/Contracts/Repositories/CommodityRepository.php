<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommodityRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface CommodityRepository extends RepositoryInterface
{
    //
}
