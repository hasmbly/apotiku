<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommodityTypeRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface CommodityTypeRepository extends RepositoryInterface
{
    //
}
