<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleUserRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface RoleUserRepository extends RepositoryInterface
{
    //
}
