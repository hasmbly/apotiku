<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TransactionRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface TransactionRepository extends RepositoryInterface
{
    //
}
