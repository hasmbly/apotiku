<?php

namespace App\Exports;

use App\Models\Commodity;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class CommodityExport implements FromCollection, WithHeadings, WithEvents, ShouldAutoSize
{
    private $start_date = null;
    private $end_date = null;

    public function __construct($start_date, $end_date)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function collection()
    {
        $from = date('Y-m-d', strtotime($this->start_date));
        $to = date('Y-m-d', strtotime($this->end_date));

        $data = Commodity::from('commodities as t1')
        ->join('bentuk_sediaans as t2', 't2.id', '=', 't1.bentuk_sediaans_id')
        ->join('commodities_types as t3', 't3.id', '=', 't1.commodities_type_id')
        ->join('produsens as t4', 't4.id', '=', 't1.produsens_id')
        ->select(
            't1.id',
            't1.created_at',
            't1.code',
            't1.name as nama_barang',
            't2.name as nama_bentuk_sediaan',
            't3.name as nama_tipe_barang',
            't1.stock',
            't1.buy_price',
            't1.sell_price',
            't1.profit',
            't4.name as nama_produsen'
        )
        ->whereBetween('t1.created_at', [$from, $to])
        ->get();

        if (count($data) > 0)
        {
            $i = 1;
            foreach ($data as $index => $value)
            {
                $data[$index]->id = $i;

                if (!isset($data[$index]->stock) || $data[$index]->stock == 0 || $data[$index]->stock == '0')
                {
                    $data[$index]->stock = '0';
                }

                if (!isset($data[$index]->buy_price) || $data[$index]->buy_price == 0 || $data[$index]->buy_price == '0')
                {
                    $data[$index]->buy_price = '0';
                }

                if (!isset($data[$index]->sell_price) || $data[$index]->sell_price == 0 || $data[$index]->sell_price == '0')
                {
                    $data[$index]->sell_price = '0';
                }

                if (!isset($data[$index]->profit) || $data[$index]->profit == 0 || $data[$index]->profit == '0')
                {
                    $data[$index]->profit = '0';
                }

                $i++;
            }
        }
        else
        {
            dd('no data found', $this->start_date, $this->end_date);
            dd($data);
        }

        return $data;
    }

    public function headings(): array
    {
        return [
            [
                'NO',
                'TANGGAL',
                'KODE BARANG',
                'NAMA BARANG',
                'BENTUK SEDIAAN',
                'SATUAN',
                'STOK',
                'HARGA BELI',
                'HARGA JUAL',
                'LABA',
                'PRODUSEN'
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $headPrimary = 'A1:K1';

                $allCell = 'A2:K300';
                $bColumns = 'B2:B300';

                // set font size
                $event->sheet->getDelegate()->getStyle($headPrimary)->getFont()->setSize(11);
                $event->sheet->getDelegate()->getStyle($headPrimary)->getFill()->setFillType('solid')->getStartColor()->setARGB('D9D9D9');

                $event->sheet->getDelegate()->getStyle($headPrimary)->getFont()
                    ->applyFromArray(
                        array(
                            'name' => 'Calibri',
                            'bold' =>  true,
                        )
                    );

                $event->sheet->getDelegate()->getStyle($bColumns)->getAlignment()->setWrapText(true);

                $event->sheet->getDelegate()->getStyle($headPrimary)->getAlignment()->setHorizontal('center');
                $event->sheet->getDelegate()->getStyle($headPrimary)->getAlignment()->setVertical('center');

                $event->sheet->getDelegate()->getStyle($allCell)->getAlignment()->setVertical('center');
                $event->sheet->getDelegate()->getStyle($allCell)->getAlignment()->setHorizontal('center');

                $rowNumber = 1;
                $event->sheet->getRowDimension($rowNumber)->setRowHeight(40);
            }
        ];
    }
}
