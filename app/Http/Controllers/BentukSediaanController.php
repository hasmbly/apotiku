<?php

namespace App\Http\Controllers;

use App\Models\BentukSediaan;
use App\Models\Commodity;
use App\Http\Requests\BentukSediaanRequest;

class BentukSediaanController extends Controller
{
    public function index(BentukSediaan $model)
    {
        return view('bentukSediaan.index', ['bentukSediaan' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('bentukSediaan.create');
    }

    public function store(BentukSediaanRequest $request, BentukSediaan $model)
    {
        $model->create($request->all());

        return redirect()->route('bentukSediaan.index')->withStatus(__('Bentuk Sediaan successfully created.'));
    }

    public function edit(BentukSediaan $bentukSediaan)
    {
        return view('bentukSediaan.edit', compact('bentukSediaan'));
    }

    public function update(BentukSediaanRequest $request, BentukSediaan  $bentukSediaan)
    {
        $bentukSediaan->update($request->all());

        return redirect()->route('bentukSediaan.index')->withStatus(__('Bentuk Sediaan successfully updated.'));
    }

    public function destroy(BentukSediaan  $bentukSediaan)
    {
        $checkData = Commodity::where('bentuk_sediaans_id', $bentukSediaan->id)->get();

        if (count($checkData) > 0)
        {
            return redirect()->route('bentukSediaan.index')->with('status_error', 'Bentuk Sediaan gagal di Hapus, Bentuk Sediaan sedang di gunakan di Data Barang!');
        }

        $bentukSediaan->delete();

        return redirect()->route('bentukSediaan.index')->withStatus(__('Bentuk Sediaan successfully deleted.'));
    }
}
