<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Commodity;
use App\Models\CommodityType;
use App\Http\Requests\CommodityRequest;

use App\Exports\CommodityExport;
use App\Imports\CommodityImport;
use App\Models\BentukSediaan;
use App\Models\Produsen;
use Maatwebsite\Excel\Facades\Excel;

class CommodityController extends Controller
{
    private $maxRow = 15;

    public function index(Commodity $model)
    {
        $data['no'] = 1;

        $data['commodities'] = $model->paginate($this->maxRow);

        $data['total_page'] = $data['commodities']->lastPage();

        $data['max_row'] = $this->maxRow;

        return view('commodities.index', $data);
    }

    public function export(Request $request)
    {
        if ($request->has('start_date') && $request->has('end_date'))
        {
            $start_date = $request->input('start_date');
            $end_date = $request->input('end_date');

            return Excel::download(new CommodityExport($start_date, $end_date), 'data_obat.xlsx');
        }
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file'))
        {
            Excel::import(new CommodityImport, $request->file('file'));

            return redirect('/commodities')->with('success', 'Barang Berhasil di Import.');
        }
    }

    public function create()
    {
        $commodityType = CommodityType::all();
        $bentukSediaan = BentukSediaan::all();
        $produsen = Produsen::all();

        return view('commodities.create',
        [
            'commodities_types' => $commodityType,
            'bentuk_sediaans' => $bentukSediaan,
            'produsens' => $produsen
        ]);
    }

    public function store(CommodityRequest $request, Commodity $model)
    {
        $model->create($request->all());

        return redirect()->route('commodities.index')->withStatus(__('Barang successfully created.'));
    }

    public function edit(Commodity $commodity)
    {
        $commodityType = CommodityType::all();
        $bentukSediaan = BentukSediaan::all();
        $produsen = Produsen::all();

        return view(
            'commodities.edit',
            [
                'commodity' => $commodity,
                'commodities_types' => $commodityType,
                'bentuk_sediaans' => $bentukSediaan,
                'produsens' => $produsen
            ]
        );
    }

    public function update(CommodityRequest $request, Commodity  $commodity)
    {
        $commodity->update($request->all());

        // Session::flash('success_message', 'Barang successfully updated.');

        // return redirect()->back();

        return redirect()->route('commodities.index')->with('success_message', 'Barang successfully updated.');
    }

    public function destroy(Commodity  $commodity)
    {
        $commodity->delete();

        return redirect()->route('commodities.index')->with('success_message', 'Barang successfully deleted.');
    }

    public function search(Request $request)
    {
            $query = $request->get('query');
            $id = $request->get('id');

            if ($query != '')
            {
                $commodities = Commodity::select("*")
                        ->orWhere('code', 'LIKE', '%'.$query.'%')
                        ->orWhere('name', 'LIKE', '%'.$query.'%')
                        ->get();
            }
            else if ($id != '')
            {
                $commodities = Commodity::find($id);
            }
            else
            {
                $commodities = Commodity::all();
            }

            return response()->json($commodities);
    }

    public function getCommodityQtyByID(Request $request)
    {
        $id = $request->get('id');

        try
        {
            $commodity = Commodity::where('id', $id)->pluck('stock');

            if (count($commodity) != 0)
            {
                return response()->json($commodity[0]);
            }
        }
        catch (Exception $exception)
        {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    public function SearchData(Request $req)
    {
        $rs_data = Commodity::from('commodities as a')
            ->join('commodities_types', 'commodities_types.id', '=', 'a.commodities_type_id')
            ->join('bentuk_sediaans', 'bentuk_sediaans.id', '=', 'a.bentuk_sediaans_id')
            ->join('produsens', 'produsens.id', '=', 'a.produsens_id')
            ->select(
            'a.*',
            'commodities_types.id as commodities_type_id',
            'bentuk_sediaans.id as bentuk_sediaans_id',
            'produsens.id as produsens_id',
            'commodities_types.name as commodityType',
            'bentuk_sediaans.name as bentukSediaan',
            'produsens.name as produsen'
            );

        $textSearch = $req->text_search;
        $filterSearch = $req->filter_search;
        $sortSearch = $req->sort_search;

        $rs_data->where(function ($q) use ($textSearch, $filterSearch)
        {
            if ($filterSearch != "" || $filterSearch != null)
            {
                $q->Where('a.' . $filterSearch, 'LIKE', '%' . $textSearch . '%');
            }
            else
            {
                $q->where('a.name', 'LIKE', '%' . $textSearch . '%');
                $q->orWhere('a.code', 'LIKE', '%' . $textSearch . '%');
            }
        });

        if ($filterSearch != "" || $filterSearch != null)
        {
            if ($sortSearch != "" || $sortSearch != null)
            {
                if ($sortSearch == "first")
                {
                    $qrData = $rs_data->orderBy('a.created_at', 'asc');
                }
                else if ($sortSearch == "last")
                {
                    $qrData = $rs_data->orderBy('a.created_at', 'desc');
                }
                else
                {
                    $qrData = $rs_data->orderBy($filterSearch, $sortSearch);
                }
            }
            else
            {
                $qrData = $rs_data->orderBy($filterSearch, 'asc');
            }
        }
        else if ($sortSearch != "" || $sortSearch != null)
        {
            if ($sortSearch == "first")
            {
                $qrData = $rs_data->orderBy('a.created_at', 'asc');
            }
            else if ($sortSearch == "last")
            {
                $qrData = $rs_data->orderBy('a.created_at', 'desc');
            }
            else
            {
                $qrData = $rs_data->orderBy('a.code', $sortSearch);
            }
        }

        $qrData = $rs_data->paginate($this->maxRow);

        $data['rs_data'] = $qrData;
        $data['pagination'] =  (string) $qrData->links();

        return json_encode($data);
    }
}
