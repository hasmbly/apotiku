<?php

namespace App\Http\Controllers;

use App\Models\CommodityType;
use App\Models\Commodity;
use App\Http\Requests\CommodityTypeRequest;

class CommodityTypeController extends Controller
{
    public function index(CommodityType $model)
    {
        return view('commodityTypes.index', ['commodityTypes' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('commodityTypes.create');
    }

    public function store(CommodityTypeRequest $request, CommodityType $model)
    {
        $model->create($request->all());

        return redirect()->route('commodityTypes.index')->withStatus(__('Satuan Jenis Barang successfully created.'));
    }

    public function edit(CommodityType $commodityType)
    {
        return view('commodityTypes.edit', compact('commodityType'));
    }

    public function update(CommodityTypeRequest $request, CommodityType  $commodityType)
    {
        $commodityType->update($request->all());

        return redirect()->route('commodityTypes.index')->withStatus(__('Satuan Jenis Barang successfully updated.'));
    }

    public function destroy(CommodityType  $commodityType)
    {
        // check if commoditues_type already used in commodities
        $getCommoditiesType = Commodity::where('commodities_type_id', $commodityType->id)->get();

        if (count($getCommoditiesType) > 0)
        {
            return redirect()->route('commodityTypes.index')->with('status_error', 'Satuan Jenis Barang gagal di Hapus, Satuan sedang di gunakan!');
        }

        $commodityType->delete();

        return redirect()->route('commodityTypes.index')->withStatus(__('Satuan Jenis Barang successfully deleted.'));
    }
}
