<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Models\Transaction;
use App\Models\TransactionItems;
use App\Models\Commodity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $penjualans = $this->getPenjualans();

        $total_barang = $this->getTotalBarang();

        $total_penjualan = $this->getTotalPenjualan();

        $total_profit = $this->getCommoditiesProfit();

        return view('dashboard',
            [
                'penjualans' => $penjualans,
                'total_barang' => $total_barang,
                'total_penjualan' => $total_penjualan,
                'total_profit' => $total_profit
            ]);
    }

    public function getCommoditiesProfit()
    {
        $get_commodities_profit = TransactionItems::whereDate('created_at', date('Y-m-d'))->get();

        $total_profit = 0;

        foreach ($get_commodities_profit as $object) {
            $profit = $object->commodity->profit * $object->qty;
            $total_profit = $total_profit + $profit;
        }

        return $total_profit;
    }

    public function getTotalPenjualan()
    {
        $total_penjualan = TransactionItems::whereDate('created_at', date('Y-m-d'))->sum('sub_total');

        return $total_penjualan;
    }

    public function getTotalBarang()
    {
        $total_barang = Commodity::count();

        return $total_barang;
    }

    public function getPenjualans()
    {
        $penjualans = TransactionItems::whereDate('created_at', date('Y-m-d'))->paginate(10);

        return $penjualans;
    }
}
