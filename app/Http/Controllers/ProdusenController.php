<?php

namespace App\Http\Controllers;

use App\Models\Produsen;
use App\Models\Commodity;
use App\Http\Requests\ProdusenRequest;

class ProdusenController extends Controller
{
    public function index(Produsen $model)
    {
        return view('produsen.index', ['produsen' => $model->paginate(15)]);
    }

    public function create()
    {
        return view('produsen.create');
    }

    public function store(ProdusenRequest $request, Produsen $model)
    {
        $model->create($request->all());

        return redirect()->route('produsen.index')->withStatus(__('Produsen successfully created.'));
    }

    public function edit(Produsen $produsen)
    {
        return view('produsen.edit', compact('produsen'));
    }

    public function update(ProdusenRequest $request, Produsen  $produsen)
    {
        $produsen->update($request->all());

        return redirect()->route('produsen.index')->withStatus(__('Produsen successfully updated.'));
    }

    public function destroy(Produsen  $produsen)
    {
        $checkData = Commodity::where('produsens_id', $produsen->id)->get();

        if (count($checkData) > 0) {
            return redirect()->route('produsen.index')->with('status_error', 'Produsen gagal di Hapus, Produsen sedang di gunakan di Data Barang!');
        }

        $produsen->delete();

        return redirect()->route('produsen.index')->withStatus(__('Produsen successfully deleted.'));
    }
}
