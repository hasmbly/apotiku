<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\TransactionItems;
use App\Models\Commodity;

use App\Http\Requests\TransactionRequest;
use App\Http\Requests\TransactionItemsRequest;
use Illuminate\Http\Request;

use PDF;

class TransactionsController extends Controller
{
    public function index(Transaction $model)
    {
        return view('transactions.index', ['transactions' => $model->paginate(15)]);
    }

    public function create($type)
    {
        return view('transactions.create', ['transactions_type' => $type]);
    }

    public function reportPenjualan(Request $request, TransactionItems $model)
    {
        $total_penjualan = 0;
        $total_profit = 0;
        $start_date = date('Y-m-d');

        if ( $request->has('start_date') )
        {
            if ( empty($request->input('start_date')) )
            {
                return redirect()->route('transactions.report_penjualan');
            }

            $start_date = $request->input('start_date');
            $start_date = date('Y-m-d', strtotime($start_date));

            $object = $model->whereDate('created_at', $start_date)->paginate(10);

            // penjualan
            $total_penjualan = TransactionItems::whereDate('created_at', $start_date)->sum('sub_total');

            // profit
            $get_commodities_profit = TransactionItems::whereDate('created_at', $start_date)->get();

            foreach ($get_commodities_profit as $obj) {
                $profit = $obj->commodity->profit * $obj->qty;
                $total_profit = $total_profit + $profit;
            }

            if ( empty($model) )
            {
                return view('transactions.report_penjualan',
                [
                    'penjualans' => $object,
                    'total_profit' => $total_profit,
                    'total_penjualan' => $total_penjualan
                ]);
            }
            else
            {
                return view('transactions.report_penjualan',
                [
                    'penjualans' => $object,
                    'total_profit' => $total_profit,
                    'total_penjualan' => $total_penjualan
                ]);
            }
        }
        else
        {
            // penjualan
            $total_penjualan = TransactionItems::sum('sub_total');

            // profit
            $get_commodities_profit = TransactionItems::all();

            foreach ($get_commodities_profit as $object) {
                $profit = $object->commodity['profit'] * $object->qty;
                $total_profit = $total_profit + $profit;
            }

            return view('transactions.report_penjualan',
            [
                'penjualans' => $model->paginate(10),
                'total_profit' => $total_profit,
                'total_penjualan' => $total_penjualan
            ]);
        }
    }


    public function store(TransactionRequest $request, Transaction $model)
    {
        $data = $model->create($request->all());

        return response()->json($data);
    }

    public function storeItems(Request $request)
    {
        $requestData = $request->all();

        if ( is_array($requestData) )
        {
            foreach ($requestData as $data) {
                $model = new TransactionItems();

                $data = $model->create($data);
            }

            return response()->json([ 'success' => true, 'message' => 'Create Transaction Items Successfully' ]);
        }
        else
        {
            $model = new TransactionItems();

            $data = $model->create($requestData);

            return response()->json([ 'success' => true, 'message' => 'Create Transaction Items Successfully' ]);
        }
    }

    public function exportPdf(TransactionItems $model)
    {
        $pdf = PDF::loadView('transactions.partials.penjualan_export', ['penjualans' => $model->all() ]);

        set_time_limit(300);

        return $pdf->stream();

        // return $pdf->download('transactions.export.pdf');
    }

    public function edit(Transaction $transaction)
    {
        return view('transactions.edit', ['transaction' => $transaction]);
    }

    public function update(TransactionRequest $request, Transaction  $transaction)
    {
        $transaction->update($request->all());

        return redirect()->route('transactions.index')->withStatus(__('Transaction successfully updated.'));
    }

    public function destroy(Transaction  $transaction)
    {
        $transaction->delete();

        return redirect()->route('transactions.index')->withStatus(__('Transaction successfully deleted.'));
    }
}
