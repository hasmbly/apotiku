<?php

namespace App\Http\Requests;

use App\Models\Commodity;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CommodityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => [
                'required', 'min:3'
            ],
            'name' => [
                'required', 'min:3'
            ],
            'bentuk_sediaans_id' => [
                'required', 'integer'
            ],
            'commodities_type_id' => [
                'required', 'integer'
            ],
            'produsens_id' => [
                'required', 'integer'
            ],
            'stock' => [
                'required', 'integer'
            ],
            'buy_price' => [
                'required', 'numeric'
            ],
            'sell_price' => [
                'required', 'numeric'
            ],
            'profit' => [
                'required', 'numeric'
            ]
        ];
    }
}
