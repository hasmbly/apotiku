<?php

namespace App\Http\Requests;

use App\Models\TransactionItems;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TransactionItemsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transactions_id' => [
                'required', 'integer'
            ],
            'commodities_id' => [
                'required', 'integer'
            ],
            'qty' => [
                'required', 'integer'
            ],
            'sub_total' => [
                'required', 'integer'
            ],
        ];
    }
}
