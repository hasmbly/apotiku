<?php

namespace App\Http\Requests;

use App\Models\Transaction;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transactions_code' => [
                'required', 'string'
            ],
            'user_id' => [
                'required', 'integer'
            ],
            'transaction_type' => [
                'required', 'integer'
            ],
            'total' => [
                'required', 'integer'
            ],
            'cash' => [
                'required', 'integer'
            ],
            'change' => [
                'required', 'integer'
            ],
        ];
    }
}
