<?php

namespace App\Imports;

use App\Models\Commodity;
use App\Models\BentukSediaan;
use App\Models\CommodityType;
use App\Models\Produsen;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CommodityImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $firstRow = true;

        foreach ($rows as $row) {

            // skip first row
            if ($firstRow)
            {
                $firstRow = false;
                continue;
            }
            if ($row[0] != null || $row[0] != "")
            {
                // check if code is exist ? 'update' : 'create'
                $getID = Commodity::where('code', '=', $row[2])->pluck('id');

                if (count($getID) > 0)
                {
                    $id = $getID[0];

                    $commodity = Commodity::find($id);
                    $commodity->timestamps = false;

                    $commodity->created_at = $row[1];
                    $commodity->code = $row[2];
                    $commodity->name = $row[3];
                    $commodity->bentuk_sediaans_id = $this->getBentukSediaansID($row[4]);
                    $commodity->commodities_type_id = $this->getCommoditiesTypeID($row[5]);
                    $commodity->stock = $row[6];
                    $commodity->buy_price = $row[7];
                    $commodity->sell_price = $row[8];
                    $commodity->profit = $row[9];
                    $commodity->produsens_id = $this->getProdusenID($row[10]);

                    $commodity->save();
                }
                else
                {
                    $commodity = new Commodity;
                    $commodity->timestamps = false;

                    $commodity->created_at = $row[1];
                    $commodity->code = $row[2];
                    $commodity->name = $row[3];
                    $commodity->bentuk_sediaans_id = $this->getBentukSediaansID($row[4]);
                    $commodity->commodities_type_id = $this->getCommoditiesTypeID($row[5]);
                    $commodity->stock = $row[6];
                    $commodity->buy_price = $row[7];
                    $commodity->sell_price = $row[8];
                    $commodity->profit = $row[9];
                    $commodity->produsens_id = $this->getProdusenID($row[10]);

                    $commodity->save();
                }
            }
            else
            {
                continue;
            }
        }
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function getBentukSediaansID($value)
    {
        if (isset($value))
        {
            $getID = BentukSediaan::where('name', 'LIKE', '%' . $value . '%')->pluck('id');

            if (count($getID) > 0)
            {
                return $getID[0];
            }
            else
            {
                dd("Maaf Data Bentuk Sediaan tidak ditemukan, Silahkan Tambahkan Data Baru Bentuk Sediaan");
            }
        }
    }

    public function getCommoditiesTypeID($value)
    {
        if (isset($value))
        {
            $getID = CommodityType::where('name', 'LIKE', '%' . $value . '%')->pluck('id');

            if (count($getID) > 0)
            {
                return $getID[0];
            }
            else
            {
                dd("Maaf Data Satuan Barang tidak ditemukan, Silahkan Tambahkan Data Baru Satuan Barang");
            }
        }
    }

    public function getProdusenID($value)
    {
        if (isset($value))
        {
            $getID = Produsen::where('name', 'LIKE', '%' . $value . '%')->pluck('id');

            if (count($getID) > 0)
            {
                return $getID[0];
            }
            else
            {
                dd("Maaf Data Produsen tidak ditemukan, Silahkan Tambahkan Data Baru Produsen");
            }
        }
    }
}
