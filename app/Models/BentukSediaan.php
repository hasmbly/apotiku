<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Commodity;

class BentukSediaan extends Model
{
    public $table = 'bentuk_sediaans';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    protected $fillable = [
        'name'
    ];

    #region commodity
    public function commodity()
    {
        return $this->hasOne('App\Models\Commodity', 'bentuk_sediaans_id');
    }
    #endregion
}
