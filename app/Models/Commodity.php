<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction;
use App\Models\TransactionItems;
use App\Models\CommodityType;
use App\Models\BentukSediaan;
use App\Models\Produsen;

/**
 * Class Commodity.
 *
 * @package namespace App\Models;
 */
class Commodity extends Model
{
    public $table = 'commodities';

    protected $primaryKey = 'id';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'code',
        'name',
        'bentuk_sediaans_id',
        'commodities_type_id',
        'stock',
        'buy_price',
        'sell_price',
        'profit',
        'produsens_id',
        'created_at'
    ];

    #region transactions
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }
    #endregion

    #region transactionItems
    public function transactionItems()
    {
        return $this->hasMany('App\Models\TransactionItems');
    }
    #endregion

    #region commodityType
    public function commodityType()
    {
        return $this->belongsTo('App\Models\CommodityType', 'commodities_type_id');
    }
    #endregion

    #region bentukSediaan
    public function bentukSediaan()
    {
        return $this->belongsTo('App\Models\BentukSediaan', 'bentuk_sediaans_id');
    }
    #endregion

    #region produsen
    public function produsen()
    {
        return $this->belongsTo('App\Models\Produsen', 'produsens_id');
    }
    #endregion
}
