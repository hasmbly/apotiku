<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Commodity;

/**
 * Class CommodityType.
 *
 * @package namespace App\Models;
 */
class CommodityType extends Model
{
    public $table = 'commodities_types';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    #region commodity
    public function commodity()
    {
        return $this->hasOne('App\Models\Commodity', 'commodities_type_id');
    }
    #endregion
}
