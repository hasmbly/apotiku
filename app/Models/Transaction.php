<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\TransactionItems;
use App\User;

/**
 * Class Transaction.
 *
 * @package namespace App\Models;
 */
class Transaction extends Model
{
    public $table = 'transactions';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'id',
		'transactions_code',
		'user_id',
		'transaction_type',
		'total',
		'cash',
        'change',
    ];

    #region commodity
    public function transactionItemsPenjualan()
    {
        return $this->hasMany('App\Models\TransactionItems', 'transactions_id')
            ->join('transactions', 'transaction_items.transactions_id', '=', 'transactions.id')
            ->where('transactions.transaction_type', '=', 1)
            ->get();
    }
    #endregion

    #region commodity
    public function transactionItems()
    {
        return $this->hasMany('App\Models\TransactionItems', 'transactions_id');
    }
    #endregion

    #region commodity
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    #endregion
}
