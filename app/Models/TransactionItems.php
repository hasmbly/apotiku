<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Transaction;
use App\Models\Commodity;


class TransactionItems extends Model
{
    public $table = 'transaction_items';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'id',
		'transactions_id',
        'commodities_id',
		'qty',
		'sub_total',
    ];

    #region commodity
    public function commodity()
    {
        return $this->belongsTo('App\Models\Commodity', 'commodities_id');
    }
    #endregion

    #region commodity
    public function transactions()
    {
        return $this->belongsTo('App\Models\Transaction', 'transactions_id');
    }
    #endregion
}
