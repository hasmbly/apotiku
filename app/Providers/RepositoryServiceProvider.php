<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Contracts\Repositories\RoleRepository::class, \App\Repositories\Eloquent\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\RoleUserRepository::class, \App\Repositories\Eloquent\RoleUserRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\CommodityRepository::class, \App\Repositories\Eloquent\CommodityRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\TransactionRepository::class, \App\Repositories\Eloquent\TransactionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\CommodityTypeRepository::class, \App\Repositories\Eloquent\CommodityTypeRepositoryEloquent::class);
        //:end-bindings:
    }
}
