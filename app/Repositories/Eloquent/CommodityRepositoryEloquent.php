<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\CommodityRepository;
use App\Models\Commodity;
use App\Validators\CommodityValidator;

/**
 * Class CommodityRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class CommodityRepositoryEloquent extends BaseRepository implements CommodityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Commodity::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CommodityValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
