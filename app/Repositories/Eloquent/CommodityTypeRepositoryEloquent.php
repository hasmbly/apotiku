<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\CommodityTypeRepository;
use App\Models\CommodityType;
use App\Validators\CommodityTypeValidator;

/**
 * Class CommodityTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class CommodityTypeRepositoryEloquent extends BaseRepository implements CommodityTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CommodityType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CommodityTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
