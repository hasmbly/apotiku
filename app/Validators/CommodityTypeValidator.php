<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class CommodityTypeValidator.
 *
 * @package namespace App\Validators;
 */
class CommodityTypeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'required' => 'name'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'required' => 'name'
        ],
    ];
}
