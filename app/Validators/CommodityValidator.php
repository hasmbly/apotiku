<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class CommodityValidator.
 *
 * @package namespace App\Validators;
 */
class CommodityValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'required'	=> 'code',
            'required'	=> 'name',
            'required'	=> 'commodity_type_id',
            'required'	=> 'stock',
            'required'	=> 'buy_price',
            'required'	=> 'sell_price',
            'required'	=> 'profit'
	    ],
        ValidatorInterface::RULE_UPDATE =>
        [
            'required'	=> 'code',
            'required'	=> 'name',
            'required'	=> 'commodity_type_id',
            'required'	=> 'stock',
            'required'	=> 'buy_price',
            'required'	=> 'sell_price',
            'required'	=> 'profit'
	    ],
    ];
}
