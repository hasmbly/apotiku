<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RoleUserValidator.
 *
 * @package namespace App\Validators;
 */
class RoleUserValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'required'	=>'	role_id',
            'required'	=>'	user_id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'required'	=>'	role_id',
            'required'	=>'	user_id',
        ],
    ];
}
