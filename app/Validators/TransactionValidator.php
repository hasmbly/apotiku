<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class TransactionValidator.
 *
 * @package namespace App\Validators;
 */
class TransactionValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'required'	=> 'commodity_id',
            'required'	=> 'user_id',
            'required'	=> 'transaction_type',
            'required'	=> 'qty',
            'required'	=> 'amount',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'required'	=> 'commodity_id',
            'required'	=> 'user_id',
            'required'	=> 'transaction_type',
            'required'	=> 'qty',
            'required'	=> 'amount',
        ],
    ];
}
