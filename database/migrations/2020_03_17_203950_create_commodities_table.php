<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCommoditiesTable.
 */
class CreateCommoditiesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Schema::disableForeignKeyConstraints();

		Schema::create('commodities', function(Blueprint $table) {
            $table->increments('id');
			$table->string('code');
			$table->string('name');
			$table->unsignedInteger('commodities_type_id');
			$table->integer('stock');
			$table->bigInteger('buy_price');
			$table->bigInteger('sell_price');
			$table->bigInteger('profit');
            $table->timestamps();

            $table->index('commodities_type_id');

            // $table->foreign('commodities_type_id')->references('id')->on('commodities_type');
        });

        // Schema::enableForeignKeyConstraints();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('commodities');
	}
}
