<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBentukSediaansIdToCommoditiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commodities', function (Blueprint $table) {
			$table->unsignedInteger('bentuk_sediaans_id')->nullable()->after('name');
            $table->unsignedInteger('produsens_id')->nullable()->after('profit');

            $table->index('bentuk_sediaans_id');
            $table->index('produsens_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commodities', function (Blueprint $table) {
            $table->dropIndex('bentuk_sediaans_id');
            $table->dropIndex('produsens_id');

            $table->dropColumn('bentuk_sediaans_id');
            $table->dropColumn('produsens_id');
        });
    }
}
