<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AddDevUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userID = DB::table('users')->insertGetId(
            array(

                'name' => 'Developer',
                'email' => 'developer.apotiku@gmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            )
        );

        DB::table('role_user')->insert([
            [
                'role_id' => 1,
                'user_id' => $userID,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 2,
                'user_id' => $userID,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 3,
                'user_id' => $userID,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
