<?php

use Illuminate\Database\Seeder;

class BentukSediaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bentuk_sediaans')->insert([
            [
                'name' => 'Obat Luar',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Obat Lokal',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Salep',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Sirup',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Tablet',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
