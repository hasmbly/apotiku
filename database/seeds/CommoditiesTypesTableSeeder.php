<?php

use Illuminate\Database\Seeder;

class CommoditiesTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('commodities_types')->insert([
            [
                'name' => 'Unit',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Strip',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Ampul',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Botol',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
