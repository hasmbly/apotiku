<?php

use Illuminate\Database\Seeder;

class ProdusenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produsens')->insert([
            [
                'name' => 'Molex',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Ifars',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Interbat',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
