<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'SUPER_ADMIN',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'ADMIN',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'OPERATOR',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
