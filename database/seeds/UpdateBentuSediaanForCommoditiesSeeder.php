<?php

use Illuminate\Database\Seeder;

class UpdateBentuSediaanForCommoditiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bentukSediaan = DB::table('bentuk_sediaans')->where('name', '=', 'Obat Lokal')->pluck('id');

        DB::table('commodities')->update(['bentuk_sediaans_id' => $bentukSediaan[0]]);
    }
}
