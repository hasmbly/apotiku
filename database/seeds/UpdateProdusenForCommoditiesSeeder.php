<?php

use Illuminate\Database\Seeder;

class UpdateProdusenForCommoditiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produsen = DB::table('produsens')->where('name', '=', 'Ifars')->pluck('id');

        DB::table('commodities')->update(['produsens_id' => $produsen[0]]);
    }
}
