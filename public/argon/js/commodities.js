$(document).ready(function () {
    //#region Commodity
    var inputBuyPrice = $("#input-buy-price");
    var inputSellPrice = $("#input-sell-price");
    var inputProfit = $("#input-profit");

    var inputBuyPriceValue = 0;
    var inputSellPriceValue = 0;
    var profit = 0;

    inputBuyPrice.on("input propertychange", function ()
    {
        inputBuyPriceValue = $(this).val();

        countProfit();
    });

    inputSellPrice.on("input propertychange", function ()
    {
        inputSellPriceValue = $(this).val();

        countProfit();
    });

    function countProfit()
    {
        profit = inputSellPriceValue - inputBuyPriceValue;

        inputProfit.val(profit);
    }
    //#endregion

    //#region Penjualan

    // variable
    var inputSearchCommodity = $(".search-commodity");
    var dropDownListCommodity = $(".search-barang-dropdown");
    var listCommodityItem = $('.list-commodities-search-items');
    var transactionTable = $('.transaction-table-main');

    var inputTotal = $('#input-total');
    var inputCash = $('#input-cash');
    var inputChange = $('#input-change');
    var inputChangeReal = $('#input-change-real');

    var buttonSave = $('.save');

    var inputTotalValue = 0;
    var inputCashValue = 0;
    var inputChangeValue = 0;
    var inpuChangeRealValue = 0;

    var buttonCloseStrukModal = $('#button-close-struk-modal');
    var buttonCetakStrukModal = $('#button-cetak-struk-modal');

    var sumTotal = 0;
    var dataTableID = [];
    var timer;

    var existingQty = 0;

    // event ketika click di dokument close 'dropDownListCommodity'
    $(document)
        .on('click', function ()
        {
            dropDownListCommodity.hide();
        });

    //#region penghitungan
    // reload halaman setelah close modal 'Struk'
    buttonCloseStrukModal
        .on('click', function () {
            location.reload();
        });

    // event listener klo ada perubahan dia mulai ngitung
    inputTotal
        .on("input propertychange", function () {
            inputTotalValue = $(this).val();

            // panggil fungsi 'countChange()' utk menghitung
            countChange();
        });

    inputCash
        .on("input propertychange", function () {
            inputCashValue = $(this).val();

            countChange();
        });

    buttonSave
        .on('click', function ()
        {
            createTransaction();
        });
    //#endregion

    inputSearchCommodity
        .on('click', function ()
        {
            dropDownListCommodity.show();
        })
        .on('keyup', function ()
        {
            // pake timer biar gk banyak dam gk bentrok ajax nya
            clearTimeout(timer);
            timer = setTimeout(function ()
            {
                dropDownListCommodity.show();

                // panggil data dengan ajax
                fetchCommoditiesData();
            }, 500);
        });

    listCommodityItem
        .on('click', 'a.commodity-item', function ()
        {
            var id = $(this).data('id');

            // isExist ID in array ? addition to qty : push to array
            if (dataTableID.length != 0)
            {
                var isExist = false;
                for (i = 0; i < dataTableID.length; i++) {

                    var value = dataTableID[i];
                    if (value == id) {

                        isExist = true;

                        // addition to qty
                        additionQty(id);

                        var itemSell = getItemSell(id);
                        var itemsQty =  getItemQty(id);

                        // set row totals
                        setRowTotalByID(id, itemSell, itemsQty);

                        //  set sum total
                        setSumTotal(itemSell, true);

                        // count change
                        countChange();

                        break;
                    }
                };

                if (!isExist)
                {
                    dataTableID.push(id);
                    fetchCommoditiesDataWithID(id);
                }
            }
            else if (dataTableID.length == 0)
            {
                dataTableID.push(id);
                fetchCommoditiesDataWithID(id);
            }
        });

    transactionTable
        .on('click', '.td-minus', function () {
            // get id from button
            var id = $(this).data('id');

            // subtition ItemQty
            var subQty = subtitutionQty(id);
            if (!subQty) return;

            // get itemQty
            var itemsQty = getItemQty(id);

            // get ItemSell
            var itemSell = getItemSell(id);

            // set Row Total
            setRowTotalByID(id, itemSell, itemsQty);

            // set sum total
            setSumTotal(itemSell, false);

            // count CHange
            if (inputCashValue != 0)
            {
                countChange();
            }
        })
        .on('click', '.td-plus', function () {
            // get id from button
            var id = $(this).data('id');

            // addition ItemQty
            additionQty(id);

            // get itemQty
            var itemsQty = getItemQty(id);

            // get ItemSell
            var itemSell = getItemSell(id);

            // set Row Total
            setRowTotalByID(id, itemSell, itemsQty);

            // set sum total
            setSumTotal(itemSell, true);

            if (inputCashValue != 0) {
                countChange();
            }
        })
        .on('click', '.td-remove', function () {
            // get id
            var id = $(this).data('id');

            // get itemQty
            var itemsQty = getItemQty(id);

            // get ItemSell
            var itemSell = getItemSell(id);

            // get row total
            var rowTotal = getRowTotal(itemSell, itemsQty);

            // set sum total
            setSumTotal(rowTotal, false);

            // count change
            countChange();

            // remove data table id in array
            removeDataTableIDArray(id);

            // remove row
            $(this).closest('tr').remove();
        });

    //#region digits()
    $.fn.digits = function () {
        return this.each(function () {
            $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        })
    }
    //#endregion

    //#region createTransaction
    function createTransaction()
    {
        var user_id = $('#user_id').val();
        var transactions_code = $('#transactions_code').val();
        var transaction_type = $('#transaction_type').val();
        var total = $('#input-total').val();
        var cash = $('#input-cash').val();
        var change = $('#input-change-real').val();

        var transactionObject =
        {
            'user_id': user_id,
            'transactions_code': transactions_code,
            'transaction_type': transaction_type,
            'total': total,
            'cash': cash,
            'change': change
        };

        console.log('object: ' + JSON.stringify(transactionObject));

        $.ajax({
            url: '/transactions',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            data: JSON.stringify(transactionObject),
            success: function (data) {
                console.log(data);

                createTransactionItems(data);
                // createTransactionItems(transactionID);
            }
        });
    }
    //#endregion

    //#region createTransactionItems
    function createTransactionItems(transactionData) {

        var transactionID = transactionData.id;
        var object = {};
        var objects = [];

        var modalObject = {};
        var modalObjects = [];

        $('.transaction-table-tr').each(function (index, item) {
            console.log('index tr: ' + index);

            // object for ajax call
            object =
            {
                'transactions_id': transactionID,
                'commodities_id': $(this).data('id'),
                'qty': parseInt($(this).find('td.items-qty').text()),
                'sub_total': parseInt($(this).find('td.items-total').text())
            };

            console.log('object: ' + JSON.stringify(object));

            objects.push(object);

            // object for modal
            modalObject =
            {
                'name': $(this).find('td.items-name').text(),
                'sell': formatRupiah($(this).find('td.items-sell').text(), 'Rp. '),
                'qty': $(this).find('td.items-qty').text(),
                'sub_total': formatRupiah($(this).find('td.items-total').text(), 'Rp. ')
            };

            modalObjects.push(modalObject);
        });

        console.log('objects: ' + JSON.stringify(objects));

        $.ajax({
            url: '/transactions/store/items',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            data: JSON.stringify(objects),
            success: function (data) {
                console.log(data);

                // setup and trigger modal function
                triggerModalStruk(transactionData, JSON.stringify(modalObjects));

                // location.reload();
            }
        });
    }
    //#endregion

    //#region
    function triggerModalStruk(transactionData, transactionItems)
    {
        transactionItems = JSON.parse(transactionItems);

        // remove html inside transaction-struk-items element
        $('.transaction-struk-items').empty();

        // foreach transactionItems
        $.each(transactionItems, function (index, item)
        {
            var dataHtml = "<div class='row justify-content-end'> \
                <div class='col-2'></div> \
                    <div class='col-lg-10 text-left text-monospace'> \
                        <strong>" + item.name + "</strong> \
                    </div> \
                    <div class='col-2'></div> \
                    <div class='col-sm-4 text-left text-monospace'> \
                        " + item.sell + " \
                    </div> \
                    <div class='col-sm-1 text-center text-monospace'> \
                        x \
                    </div> \
                    <div class='col-sm-1 text-center text-monospace'> \
                        " + item.qty + " \
                    </div> \
                    <div class='col-sm-4 text-right text-monospace'> \
                        " + item.sub_total + " \
                    </div> \
                </div> \
            ";

            $('.transaction-struk-items').append(dataHtml);
        });

        $('.struk-code').html(transactionData.transactions_code);
        $('.struk-date').html(transactionData.created_at);

        $('.struk-total').html(formatRupiah(transactionData.total, 'Rp. '));
        $('.struk-tunai').html(formatRupiah(transactionData.cash, 'Rp. '));
        $('.struk-change').html(formatRupiah(transactionData.change, 'Rp. '));

        $('#struk-modal').modal({
            handleUpdate: true,
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }
    //#endregion

    //#region formatRupiah
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan)
        {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }
    //#endregion

    //#region countChange
    function countChange()
    {
        inputTotalValue = inputTotal.val();

        inputCashValue = inputCash.val();

        inputChangeValue = inputCashValue - inputTotalValue;

        if (isNaN(inputChangeValue) || parseInt(inputChangeValue) < 0)
        {
            inputChange.addClass('is-invalid');

            var change = formatRupiah(inputChangeValue.toString(), 'Rp. -');

            buttonSave.prop('disabled', true);
        }
        else
        {
            inputChange.removeClass('is-invalid');

            var change = formatRupiah(inputChangeValue.toString(), 'Rp. ');

            buttonSave.prop('disabled', false);
        }

        // display
        inputChange.val(change);

        // real
        inputChangeReal.val(inputChangeValue);
    }
    //#endregion

    //#region setRowTotalByID
    function setRowTotalByID(id, itemSell, itemsQty)
    {
        var rowTotal = getRowTotal(itemSell, itemsQty);

        $('.transaction-table-main #items-total-' + id).text(rowTotal);
    }
    //#endregion

    //#region additionQty
    function additionQty(id)
    {
        var qty = getItemQty(id);

        var qtyToInt = parseInt(qty);
        qtyToInt++;

        // Check if addition to Qty is not greater than existing Qty
        clearTimeout(timer);
        timer = setTimeout(function () {
            var availablityCommodityQty = compareCommodityQty(qtyToInt, id);

            if (!availablityCommodityQty)
            {
                console.log('sorry you reach maximum qty of commodity');

                showAlertMessage('Maaf Quantity tidak bisa lebih dari Stok Barang', false);

                return;
            }
        }, 500);

        $('.transaction-table-main #items-qty-' + id).text(qtyToInt);
    }
    //#endregion

    //#region subtitutionQty
    function subtitutionQty(id) {
        var qty = getItemQty(id);

        if (qty == 1)
            return false;

        var qtyToInt = parseInt(qty);
        qtyToInt--;

        $('.transaction-table-main #items-qty-' + id).text(qtyToInt);

        return true;
    }
    //#endregion

    //#region getItemSellByID
    function getItemSell(id)
    {
        var itemSell = $('.transaction-table-main #items-sell-' + id).text();
        return parseInt(itemSell);
    }
    //#endregion

    //#region getItemQtyByID
    function getItemQty(id)
    {
        var itemQty = $('.transaction-table-main #items-qty-' + id).text();
        return parseInt(itemQty);
    }
    //#endregion

    //#region removeDataTableIDArray
    function removeDataTableIDArray(id)
    {
        var index = dataTableID.indexOf(id);
        if (index > -1) {
            dataTableID.splice(index, 1);
        }
    }
    //#endregion

    //#region fetchCommoditiesData
    function fetchCommoditiesData()
    {
        listCommodityItem.html('');

        var query = inputSearchCommodity.val();

        var tmpDiv = "<div class='commodity-content'></div>";

        listCommodityItem.html(tmpDiv);

        $.ajax({
            url: "/commodities/search",
            method: 'GET',
            data: { query: query },
            type: 'json',
            success: function (data)
            {
                var result = '';

                $.each(data, function (key, value)
                {
                    result += setResultItemCommoditySearch(value);
                });

                $('.commodity-content').replaceWith(result);
            }
        })
    }
    //#endregion

    //#region fetchCommoditiesDataWithID
    function fetchCommoditiesDataWithID(id)
    {
        $.ajax({
            url: "/commodities/search",
            method: 'GET',
            data: { id: id },
            type: 'json',
            success: function (data)
            {
                tableRowTransaction.init(data);

                setSumTotal(data.sell_price, true);

                // count CHange
                if (inputCashValue != 0) {
                    countChange();
                }
            }
        })
    }
    //#endregion

    //#region setResultItemCommoditySearch
    function setResultItemCommoditySearch(data)
    {
        var dataWithtHTML = "<a href='#' class='dropdown-item commodity-item' data-id=" + data.id + "> \
                    <span>" + data.name + "</span> \
                    </a>";

        return dataWithtHTML;
    }
    //#endregion

    //#region getRowTotal
    function getRowTotal(itemsSellPrice, itemsQty)
    {
        return itemsSellPrice * itemsQty;
    }
    //#endregion

    //#region setSumTotal
    function setSumTotal(itemsSellPrice, isAddition) {
        var sellPrice = parseInt(itemsSellPrice);

        if (isAddition)
        {
            sumTotal = sumTotal + sellPrice;
        } else
        {
            sumTotal = sumTotal - sellPrice;
        }

        inputTotal.val(sumTotal);
    }
    //#endregion

    //#region getCommodityQtyByID
    function getCommodityQtyByID(commodityID)
    {
        var existingQty = 0;

        $.ajax({
            url: "/commodities/getCommodityQtyByID",
            method: 'GET',
            async: false,
            data: { id: commodityID },
            type: 'json',
            success: function (data) {
                existingQty = data;
            }
        });

        return existingQty;
    }
    //#endregion

    //#region compareCommodityQty
    function compareCommodityQty(requestQty, commodityID)
    {
        var existingQty = getCommodityQtyByID(commodityID);

        if (requestQty > existingQty)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    //#endregion

    //#region
    function showAlertMessage(message, isSuccess = false)
    {
        var status = status = isSuccess ? 'success' : 'warning';

        var data = "<div class='alert alert-" + status + " alert - dismissible fade show' role='alert'> \
        " + message + "\
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'> \
            <span aria-hidden='true'>&times;</span> \
        </button>\
        </div>";

        $('.alert-message').append(data);
    }
    //#endregion

    //#region tableRowTransaction
    var tableRowTransaction = {
        transactionTable: $('.transaction-table'),
        code: '',
        qty: 1,

        init: function( data ) {
            this.addTransaction(data);
        },

        addTransaction: function(data)
        {
            if (tableRowTransaction.code != data.code) {
                var row = "<tr class='transaction-table-tr' data-id='" + data.id + "'> \
                    <td class='items items-code'>" + data.code + "</td> \
                    <td class='items items-name'>" + data.name + "</td> \
                    <td class='items items-sell' id='items-sell-" + data.id + "'>" + data.sell_price + "</td> \
                    <td> X </td> \
                    <td class='items items-qty' id='items-qty-" + data.id + "'>" + tableRowTransaction.qty + "</td> \
                    <td class='items items-total' id='items-total-" + data.id + "'>" + data.sell_price + "</td> \
                    <td class='text-right'> \
                    <button class='btn btn-icon btn-warning td-minus' data-id=" + data.id + " type='button'> \
                    <span class='btn-inner--icon'><i class='ni ni-fat-delete'></i></span> \
                    </button> \
                    <button class='btn btn-icon btn-success td-plus' data-id=" + data.id + " type='button'> \
                    <span class='btn-inner--icon'><i class='ni ni-fat-add'></i></span> \
                    </button> \
                    <button class='btn btn-icon btn-danger td-remove' data-id=" + data.id + " type='button'> \
                    <span class='btn-inner--icon'><i class='ni ni-fat-remove'></i></span> \
                    </button> \
                    </td> \
                    </tr>"
                    ;

                tableRowTransaction.transactionTable.append(row);
            }
        }
    };
    //#endregion

    //#endregion
});
