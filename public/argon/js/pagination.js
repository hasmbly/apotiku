'use strict';

var pagination = (function () {

    // property
    var config = {
        // default
        btnSave: $('#btnSave'),
        btnSubmitAdd: $('#btnSubmitAdd'),
        totalPage: $("#total_page"),

        old_text_search: $("#old_search_text"),
        text_search_element: $("#text-search"),
        sort_search_element: $("#sort-search"),
        filter_search_element: $("#filter-search"),

        text_search_value: "",
        sort_search_value: "",
        filter_search_value: "",

        // custom
        csrf_token: null,
        colName: [],
        max_row: null,
        url: null
    };

    // init
    var init = function (settings) {
        config.csrf_token = settings.csrf_token;
        config.colName = settings.colName;
        config.max_row = settings.max_row;
        config.url = settings.url;

        bindFunction();
    };

    // Bind Function
    var bindFunction = function () {
        config.btnSave.click(function () {
            config.btnSubmitAdd.click();
        });

        config.text_search_element.change(function () {
            config.text_search_value = $(this).val();

            search_process();
        });

        config.sort_search_element.change(function () {
            config.sort_search_value = $(this).val();

            search_process();
        });

        config.filter_search_element.change(function () {
            config.filter_search_value = $(this).val();

            search_process();
        });

        $(document).on("click", ".data-box .card-footer .pagination li", function (e) {
            e.preventDefault();

            var curentPage = $('.data-box .card-footer .pagination li.active').text();
            var page = $.trim($(this).text());
            var liIndex = $(this).index();

            config.totalPage.val();

            console.log(page)

            if (isNaN(page)) {
                if (page === '›') {
                    page = parseInt(curentPage) + 1;

                    if (page > config.totalPage) {
                        page = parseInt(config.totalPage);
                    }
                }

                if (page === '‹') {
                    page = parseInt(curentPage) - 1;

                    if (page === 0) {
                        page = 1;
                    }
                }

                if (page === '...') {
                    page = parseInt(curentPage)
                }

                $('.data-box .card-footer .pagination li').each(function () {
                    var liPage = $(this).text();
                    var liPageIndex = $(this).index();

                    if (page == liPage) {
                        $('.data-box .card-footer .pagination li').removeClass('active');
                        $('.data-box .card-footer .pagination li').eq(liPageIndex).addClass('active');
                    }
                });
            } else {

                $('.data-box .card-footer .pagination li').removeClass('active');
                $('.data-box .card-footer .pagination li').eq(liIndex).addClass('active');
            }

            pagination_process(parseInt(page));
        });
    };

    // Search datatable
    var search_process = function () {
        search_and_pagination_api(true);
    };

    // Pagination datatable
    var pagination_process = function (page) {
        search_and_pagination_api(false, page);
    };

    // Menampilkan data datatable
    var show_data = function (dt, page, colName) {
        var tbl = '';
        page = (page * config.max_row) - config.max_row;

        if (dt.length > 0) {
            $.each(dt, function (x, y) {
                page = page + 1;
                tbl += `
                    <tr>
                        <td>`+ page + `</td>
                `;

                if (colName.length > 0) {
                    $.each(colName, function (_i, v) {
                        var concatColName = ('y.' + v);
                        var parserColName = concatColName;
                        var cName = eval(parserColName);

                        if (v == "stock")
                        {
                            tbl += `<td>` + cName + ` pcs</td>`;
                        }
                        else if (v == "buy_price" || v == "sell_price" || v == "profit")
                        {
                            tbl += `<td>Rp. ` + cName + `</td>`;
                        }
                        else
                        {
                            tbl += `<td>` + cName + `</td>`;
                        }
                    });
                }

                tbl += `
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <form action="commodities/` + y.id + `" method="post">
                                        <input type='hidden' name='_token' value='` + config.csrf_token + `'>
                                        <input type='hidden' name='_method' value='delete'>

                                        <a href="commodities/` + y.id + `/edit" class="dropdown-item edit" data-id="` + y.id + `">Edit</a>
                                        <button type="button" class="dropdown-item delete" data-id="` + y.id + `" onclick="confirm('Are you sure you want to delete this Commodity?') ? this.parentElement.submit() : ''">
                                            Delete
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                `;
            });

            $('.data-box tbody').html(tbl);
            $('[data-toggle="tooltip"]').tooltip();
            $('.data-box tbody td:contains(null)').html(' ');
        } else {
            alert_msg('Data tidak ditemukan!');
        }
    };

    // search kelas api
    var search_and_pagination_api = function (search_process = false, page = 0) {

        search_process == true ? $("#text-search").val(config.text_search_value) : "";

        showLoading();

        $.ajax({
            type: 'POST',
            url: config.url,
            headers: { "X-CSRF-TOKEN": config.csrf_token },
            dataType: 'JSON',
            data: {
                'page': page,
                'text_search': search_process == true ? config.text_search_value : config.old_text_search.val(),
                'filter_search': config.filter_search_value,
                'sort_search': config.sort_search_value,
            },
            success: function (msg) {

                var rs = msg.rs_data;
                var dt = rs["data"];

                show_data(dt, 1, config.colName);

                if (dt.length > 0) {
                    $('.data-box .card-footer #pagination').html($(msg.pagination));
                    search_process == true ? config.old_text_search.val(config.text_search_value) : "";
                }
            },
            error: function (xhr) {
                read_error(xhr);
            },
            complete: function (_xhr) {
                closeLoading();
            }
        });
    };

    return {
        config: config,
        init: init
    };

})();
