@extends('layouts.app', ['title' => __('Data Barang')])

@section('content')
    @include('commodities.partials.header', ['title' => __('Tambah Data Barang')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Data Barang') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('commodities.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('commodities.store') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Data information') }}</h6>
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group{{ $errors->has('code') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-code">{{ __('Kode Barang') }}</label>
                                            <input type="text" name="code" id="input-code" class="form-control form-control-alternative{{ $errors->has('code') ? ' is-invalid' : '' }}" placeholder="{{ __('Kode Barang') }}" value="{{ old('code') }}" required autofocus>

                                            @if ($errors->has('code'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('code') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Nama Barang') }}</label>
                                            <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nama Barang') }}" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group{{ $errors->has('bentuk_sediaans_id') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-bentuk_sediaans_id">{{ __('Bentuk Sediaan') }}</label>
                                            <select name="bentuk_sediaans_id" id="input-bentuk_sediaans_id" class="form-control form-control-alternative{{ $errors->has('bentuk_sediaans_id') ? ' is-invalid' : '' }}" placeholder="{{ __('Pilih Bentuk Sediaan') }}" value="{{ old('bentuk_sediaans_id') }}" required autofocus>
                                                @foreach ($bentuk_sediaans as $bentuk_sediaan)
                                                    <option value="{{ $bentuk_sediaan->id }}">{{ $bentuk_sediaan->name }}</option>
                                                @endforeach
                                            </select>

                                            @if ($errors->has('bentuk_sediaans_id'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('bentuk_sediaans_id') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group{{ $errors->has('produsens_id') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-produsens_id">{{ __('Produsen') }}</label>
                                            <select name="produsens_id" id="input-produsens_id" class="form-control form-control-alternative{{ $errors->has('produsens_id') ? ' is-invalid' : '' }}" placeholder="{{ __('Pilih Bentuk Sediaan') }}" value="{{ old('produsens_id') }}" required autofocus>
                                                @foreach ($produsens as $produsen)
                                                    <option value="{{ $produsen->id }}">{{ $produsen->name }}</option>
                                                @endforeach
                                            </select>

                                            @if ($errors->has('produsens_id'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('produsens_id') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group{{ $errors->has('stock') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-stock">{{ __('Stok') }}</label>
                                            <input type="number" min="0" name="stock" id="input-stock" class="form-control form-control-alternative{{ $errors->has('stock') ? ' is-invalid' : '' }}" placeholder="{{ __('Stok') }}" value="{{ old('stock') }}" required autofocus>

                                            @if ($errors->has('stock'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('stock') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group{{ $errors->has('commodities_type_id') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-commodities_type_id">{{ __('Satuan') }}</label>
                                            <select name="commodities_type_id" id="input-commodities_type_id" class="form-control form-control-alternative{{ $errors->has('commodities_type_id') ? ' is-invalid' : '' }}" placeholder="{{ __('Pilih Satuan Barang') }}" value="{{ old('commodities_type_id') }}" required autofocus>
                                                @foreach ($commodities_types as $commodities_type)
                                                    <option value="{{ $commodities_type->id }}">{{ $commodities_type->name }}</option>
                                                @endforeach
                                            </select>

                                            @if ($errors->has('commodities_type_id'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('commodities_type_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group{{ $errors->has('buy_price') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-buy_price">{{ __('Harga Beli') }}
                                                <span class="badge badge-pill badge-primary">Satuan</span>
                                            </label>
                                            <input type="number" min="0" name="buy_price" id="input-buy-price" class="form-control form-control-alternative{{ $errors->has('buy_price') ? ' is-invalid' : '' }}" placeholder="{{ __('Harga Beli') }}" value="{{ old('buy_price') }}" required autofocus>

                                            @if ($errors->has('buy_price'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('buy_price') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group{{ $errors->has('sell_price') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-sell_price">{{ __('Harga Jual') }}
                                                <span class="badge badge-pill badge-primary">Satuan</span>
                                            </label>
                                            <input type="number" min="0" name="sell_price" id="input-sell-price" class="form-control form-control-alternative{{ $errors->has('sell_price') ? ' is-invalid' : '' }}" placeholder="{{ __('Harga Jual') }}" value="{{ old('sell_price') }}" required autofocus>

                                            @if ($errors->has('sell_price'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('sell_price') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                                            <div class="form-group{{ $errors->has('profit') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-profit">{{ __('Profit') }}
                                                    <span class="badge badge-pill badge-primary">Satuan</span>
                                                </label>
                                                <input type="number" name="profit" id="input-profit" class="form-control form-control-muted{{ $errors->has('profit') ? ' is-invalid' : '' }}" placeholder="{{ __('Profit') }}" value="{{ old('profit') }}" required autofocus>

                                                @if ($errors->has('profit'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('profit') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        @else
                                            <input type="hidden" name="profit" id="input-profit" class="form-control form-control-muted{{ $errors->has('profit') ? ' is-invalid' : '' }}" placeholder="{{ __('Profit') }}" value="{{ old('profit') }}" required autofocus>
                                        @endif
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
