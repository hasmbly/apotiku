@extends('layouts.app', ['title' => __('Data Barang')])

@section('content')
    @include('commodities.partials.header', ['title' => __('List Data Barang')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow data-box">
                    <div class="card-header border-0">
                        <div class="row align-items-center">

                            {{-- <div class="col-4 text-right">
                                <a href="{{ route('commodities.create') }}" class="btn btn-sm btn-primary">{{ __('Tambah') }}</a>
                            </div> --}}

                            <div class="col-12 text-right">

                                <a href="{{ route('commodities.create') }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{ __('Tambah') }}</a>

                                <!-- Search form -->
                                <div class="input-group float-right input-group-alternative mb-4" style="width: 200px; margin-right:10px;">
                                    <input name="" id="text-search" type="text" class="form-control form-control-alternative" placeholder="Search..." aria-label="" aria-describedby="search">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="search"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>

                                <!-- Filter by option -->
                                <div class="input-group float-right input-group-alternative mb-4" style="width: 200px; margin-right:10px;">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-filter"></i></span>
                                    </div>
                                    <select id="filter-search" class="form-control form-control-alternative" autofocus>
                                        <option disabled selected hidden>Filter</option>
                                        <option value="name">Nama Barang</option>
                                        <option value="code">Kode Barang</option>
                                        <option value="stock">Stok Barang</option>
                                        <option value="buy_price">Harga Beli Barang</option>
                                        <option value="sell_price">Harga Jual Barang</option>
                                        @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                                            <option value="profit">Profit Barang</option>
                                        @endif
                                    </select>
                                </div>

                                <!-- Sorted by option -->
                                <div class="input-group float-right input-group-alternative mb-4" style="width: 200px; margin-right:10px;">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-sort-amount-up"></i></span>
                                    </div>
                                    <select id="sort-search" class="form-control form-control-alternative" autofocus>
                                        <option disabled selected hidden>Urutkan</option>
                                        <option value="asc">A - Z</option>
                                        <option value="desc">Z - A</option>
                                        <option value="first">Data Terlama</option>
                                        <option value="last">Data Terbaru</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        {{-- Alert Status : Create, Edit --}}
                        <!-- Notif for search datatable -->
                        <div class="notif-msg" id="notif-msg"></div>

                        @if (session('error_message') || session('success_message'))
                            <div class="alert {{ session('error_message') ? ' alert-danger' : 'alert-success' }} alert-dismissible fade show" role="alert">
                                {{ session('error_message') ? session('error_message') : session('success_message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive card-body">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light" id="header">
                                <tr>
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">{{ __('Kode Barang') }}</th>
                                    <th scope="col">{{ __('Nama Barang') }}</th>
                                    <th scope="col">{{ __('Bentuk Sediaan') }}</th>
                                    <th scope="col">{{ __('Satuan') }}</th>
                                    <th scope="col">{{ __('Stok') }}</th>
                                    <th scope="col">{{ __('Harga Beli') }}</th>
                                    <th scope="col">{{ __('Harga Jual') }}</th>
                                    @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                                        <th scope="col">{{ __('Profit') }}</th>
                                        <th scope="col">{{ __('Produsen') }}</th>
                                    @endif
                                    <th scope="col">{{ __('Tanggal') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($commodities as $commodity)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $commodity->code }}</td>
                                        <td>{{ $commodity->name }}</td>
                                        <td>{{ $commodity->bentukSediaan->name ?? 'Empty' }}</td>
                                        <td>{{ $commodity->commodityType->name }}</td>
                                        <td>{{ $commodity->stock }} pcs</td>
                                        <td>Rp. {{ number_format($commodity->buy_price, 0,',','.') }}</td>
                                        <td>Rp. {{ number_format($commodity->sell_price, 0,',','.') }}</td>
                                        @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                                            <td>Rp. {{ number_format($commodity->profit, 0,',','.') }}</td>
                                            <td>{{ $commodity->produsen->name ?? 'Empty' }}</td>
                                        @endif
                                        <td>{{ $commodity->created_at->format('d/m/Y H:i') }}</td>
                                        @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <form action="{{ route('commodities.destroy', $commodity) }}" method="post">
                                                            @csrf
                                                            @method('delete')

                                                            <a class="dropdown-item edit" data-id={{ $commodity->id }} href="{{ route('commodities.edit', $commodity) }}">{{ __('Edit') }}</a>
                                                            <button type="button" class="dropdown-item delete" data-id={{ $commodity->id }} onclick="confirm('{{ __("Are you sure you want to delete this Commodity?") }}') ? this.parentElement.submit() : ''">
                                                                {{ __('Delete') }}
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                    @php
                                        $no++;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <input type="hidden" name="total_page" id="total_page" value="{{ $total_page }}">
                    <input type="hidden" name="old_search_text" id="old_search_text" value="">

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            <div id="pagination">
                                {{ $commodities->links() }}
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')

<script>
    $(document).ready(function () {
        pagination.init({
            csrf_token: '{{ csrf_token() }}',
            colName: ['code', 'name', 'bentukSediaan', 'commodityType', 'stock', 'buy_price', 'sell_price', 'profit', 'produsen', 'created_at'], // <-- nama nama kolom yang sama persis dgn table di database
            max_row: '{{ $max_row }}',
            url: '{{ url("commodities/search_commodity") }}'
        });
    });
</script>

@endpush
