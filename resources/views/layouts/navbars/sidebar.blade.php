@include('layouts.partials.modal_export')
@include('layouts.partials.modal_import')

<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-2" href="{{ route('home') }}">
            {{-- <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="..."> --}}
            <img alt="Apotek Mita Farma" src="{{ asset('argon') }}/img/brand/logo_mita_farma_original.jpeg">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/blank_user_photo.png">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            {{-- <img src="{{ asset('argon') }}/img/brand/blue.png"> --}}
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            {{-- <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form> --}}
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('home') ? 'active' : '' }}" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Dashboard') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('transactions.create') ? 'active' : '' }}" href="#navbar-transaction" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-examples">
                        <i class="ni ni-shop text-info"></i>
                        <span class="nav-link-text">{{ __('Transaksi') }}</span>
                    </a>
                    <div class="collapse" id="navbar-transaction">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('transactions.create', 1) }}">
                                    <i class="ni ni-cart text-success"></i> {{ __('Penjualan') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Management</h6>
            <!-- Navigation -->
            <ul class="navbar-nav mb-md-3">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('transactions.report.penjualan') ? 'active' : '' }}" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-examples">
                        <i class="ni ni-single-copy-04"></i>
                        <span class="nav-link-text">{{ __('Laporan') }}</span>
                    </a>
                    <div class="collapse" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('transactions.report_penjualan') }}">
                                    <i class="ni ni-cart text-success"></i> {{ __('Penjualan') }}
                                </a>
                            </li>
                            @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('transactions.report.pembelian', 2) }}">
                                    <i class="ni ni-box-2 text-warning"></i> {{ __('Pembelian') }}
                                </a>
                            </li> --}}
                            @endif
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('commodities.index') ? 'active' : '' }}" href="{{ route('commodities.index') }}">
                        <i class="ni ni-books text-blue"></i> {{ __('Barang') }}
                    </a>
                </li>
                @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('user.index') ? 'active' : '' }}" href="{{ route('user.index') }}">
                            <i class="ni ni-circle-08 text-pink"></i> {{ __('Pengguna') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('commodityTypes.index') ? 'active' : '' }}" href="{{ route('commodityTypes.index') }}">
                            <i class="ni ni-collection text-blue"></i> {{ __('Satuan Barang') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('bentukSediaan.index') ? 'active' : '' }}" href="{{ route('bentukSediaan.index') }}">
                            <i class="ni ni-box-2 text-cyan"></i> {{ __('Bentuk Sediaan') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('produsen.index') ? 'active' : '' }}" href="{{ route('produsen.index') }}">
                            <i class="ni ni-book-bookmark text-indigo"></i> {{ __('Produsen') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link {{ Route::is('commodities.export') ? 'active' : '' }}" data-toggle="modal" data-target="#modal-export">
                            <i class="fas fa-download text-red"></i> {{ __('Export Data') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link {{ Route::is('commodities.import') ? 'active' : '' }}" data-toggle="modal" data-target="#modal-import">
                            <i class="fas fa-upload text-yellow"></i> {{ __('Import Data') }}
                        </a>
                    </li>
                @elseif ( auth()->user()->hasRole("ADMIN") )
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('commodityTypes.index') ? 'active' : '' }}" href="{{ route('commodityTypes.index') }}">
                            <i class="ni ni-collection text-blue"></i> {{ __('Satuan Barang') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('bentukSediaan.index') ? 'active' : '' }}" href="{{ route('bentukSediaan.index') }}">
                            <i class="ni ni-box-2 text-cyan"></i> {{ __('Bentuk Sediaan') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('produsen.index') ? 'active' : '' }}" href="{{ route('produsen.index') }}">
                            <i class="ni ni-book-bookmark text-indigo"></i> {{ __('Produsen') }}
                        </a>
                    </li>
                @endif
                {{-- <li class="nav-item">
                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
                        <i class="ni ni-spaceship"></i> Getting started
                    </a>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>

@push('js')
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
@endpush
