<!-- Modal Add -->
<div class="modal fade" id="modal-import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-header">Import Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('commodities.import') }}" method="post" enctype="multipart/form-data" autocomplete="off">
            @csrf

            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-warning">
                        <h4 class="alert-heading">Note!</h4>
                        <p><strong>Sebelum import file yang harus di perhatikan terhadap data di dalamnya, sebagai berikut: </strong></p>
                        <ul>
                            <li>
                                pastikan data master: <strong>"bentuk sediaa, satuan, produsen"</strong> sudah di input di aplikasi
                            </li>
                            <li>
                                untuk kolom yang jenis nya Angka: <strong>"stok, harga beli, harga jual, laba"</strong> jika ingin dikosongkan harap di isi dengan nilai 0
                            </li>
                            <li>
                                untuk tanggal formatnya harus general. ex: "2020-04-05 03:15:57"
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group input-group-alternative">
                            {{-- <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div> --}}
                            <input class="form-control form-control-alternative" name="file" placeholder="Upload File" type="file" autofocus>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" style="display: none;" class="btn btn-primary" id="btnSubmitImportSave"></button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="button-save" onclick="document.getElementById('btnSubmitImportSave').click()">Submit</button>
      </div>
    </div>
  </div>
</div>
