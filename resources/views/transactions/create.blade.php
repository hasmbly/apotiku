@extends('layouts.app', ['title' => __('Penjualan')])

@section('content')
    @include('transactions.partials.header', ['title' => __('Kasir Penjualan')])
    @include('transactions.partials.struk')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-7">
                                <ul class="navbar-nav align-items-left d-none d-md-flex">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link pr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <div class="form-group mb-0">
                                                <div class="input-group input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                                    </div>
                                                    <input class="form-control search-commodity" placeholder="Cari Kode atau Nama Barang" type="text">
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-left search-barang-dropdown">
                                            <div class=" dropdown-header noti-title">
                                                <h6 class="text-overflow m-0">{{ __('List Barang!') }}</h6>
                                            </div>
                                            <div class="list-commodities-search-items"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            {{-- <div class="col-4">
                                <a href="{{ route('transactions.index') }}" class="btn btn-primary">{{ __('Tambahkan') }}</a>
                            </div> --}}
                        </div>
                    </div>

                    <div class="card-body">
                        <form autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Data Penjualan') }}</h6>

                            <div class="alert-message"></div>

                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                        <div class="table-responsive">
                            <table class="table align-items-center table-flush transaction-table-main">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('Kode') }}</th>
                                        <th scope="col">{{ __('Nama') }}</th>
                                        <th scope="col">{{ __('Harga') }}</th>
                                        <th scope="col"></th>
                                        <th scope="col">{{ __('Qty') }}</th>
                                        <th scope="col">{{ __('Sub-Total') }}</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody class="transaction-table">
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                {{-- {{ $commodities->links() }} --}}
                            </nav>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                        </div>
                    </div>

                    <div class="card-body">
                        <form autocomplete="off">
                            @csrf

                            <input type="hidden" id="transactions_code" name="transactions_code" value="PJ-{{ now()->timestamp }}">
                            <input type="hidden" id="transaction_type" name="transaction_type" value="1">
                            <input type="hidden" id="user_id" name="user_id" value="{{ auth()->user()->id }}">

                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group{{ $errors->has('total') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-total">{{ __('Total') }}</label>
                                        <input type="number" min="0" name="total" id="input-total" class="form-control form-control-alternative{{ $errors->has('total') ? ' is-invalid' : '' }}" placeholder="{{ __('Total') }}" value="{{ old('total') }}" required autofocus disabled>

                                        @if ($errors->has('total'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('total') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group{{ $errors->has('cash') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-cash">{{ __('Tunai') }}</label>
                                        <input type="number" min="0" name="cash" id="input-cash" class="form-control form-control-alternative{{ $errors->has('cash') ? ' is-invalid' : '' }}" placeholder="{{ __('Tunai') }}" value="{{ old('cash') }}" required autofocus>

                                        @if ($errors->has('cash'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('cash') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group{{ $errors->has('change') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-change">{{ __('Kembali') }}</label>

                                        <input type="hidden" name="change" id="input-change-real" class="form-control form-control-muted{{ $errors->has('change') ? ' is-invalid' : '' }}" placeholder="{{ __('Kembali') }}" value="{{ old('change') }}" required autofocus disabled>

                                        <input type="text" name="change-display" id="input-change" class="form-control form-control-muted{{ $errors->has('change') ? ' is-invalid' : '' }}" placeholder="{{ __('Kembali') }}" value="{{ old('change') }}" required autofocus disabled>

                                        @if ($errors->has('change'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('change') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-success mt-4 save" disabled>{{ __('Simpan') }}</button>
                                {{-- <button type="button" class="btn btn-primary mt-4" data-toggle="modal" data-target="#struk-modal">{{ __('Cetak Struk') }}</button> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
