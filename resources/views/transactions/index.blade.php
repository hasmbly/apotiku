@extends('layouts.app', ['title' => __('Data Barang')])

@section('content')
    @include('layouts.headers.default')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Data Barang') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('commodities.create') }}" class="btn btn-sm btn-primary">{{ __('Tambah') }}</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">{{ __('Kode Barang') }}</th>
                                    <th scope="col">{{ __('Nama Barang') }}</th>
                                    <th scope="col">{{ __('Satuan') }}</th>
                                    <th scope="col">{{ __('Stok') }}</th>
                                    <th scope="col">{{ __('Harga Beli') }}</th>
                                    <th scope="col">{{ __('Harga Jual') }}</th>
                                    <th scope="col">{{ __('Profit') }}</th>
                                    <th scope="col">{{ __('Tanggal') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($commodities as $commodity)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $commodity->code }}</td>
                                        <td>{{ $commodity->name }}</td>
                                        <td>{{ $commodity->commodityType->name }}</td>
                                        <td>{{ $commodity->stock }} pcs</td>
                                        <td>Rp. {{ $commodity->buy_price }}</td>
                                        <td>Rp. {{ $commodity->sell_price }}</td>
                                        <td>Rp. {{ $commodity->profit }}</td>
                                        <td>{{ $commodity->created_at->format('d/m/Y H:i') }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <form action="{{ route('commodities.destroy', $commodity) }}" method="post">
                                                        @csrf
                                                        @method('delete')

                                                        <a class="dropdown-item" href="{{ route('commodities.edit', $commodity) }}">{{ __('Edit') }}</a>
                                                        <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this Commodity?") }}') ? this.parentElement.submit() : ''">
                                                            {{ __('Delete') }}
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                        $no++;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $commodities->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
