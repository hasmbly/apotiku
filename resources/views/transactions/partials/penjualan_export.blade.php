<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <style>
        table tr td,
		table tr th{
			font-size: 9pt;
		},
        .page-break {
            page-break-after: always;
        }
        </style>

        <left>
            <h5>Apotek Mita Farma</h4>
        </left>

        <center>
            <h5>Laporan Penjualan</h4>
        </center>

        <br />

        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">{{ __('No') }}</th>
                        <th scope="col">{{ __('Kode Barang') }}</th>
                        <th scope="col">{{ __('Nama Barang') }}</th>
                        <th scope="col">{{ __('Harga') }}</th>
                        <th scope="col">{{ __('Jumlah') }}</th>
                        <th scope="col">{{ __('Sub Total') }}</th>
                        <th scope="col">{{ __('Profit') }}</th>
                        <th scope="col">{{ __('Tanggal') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @if (!empty($penjualans))
                        @foreach ($penjualans as $penjualan)
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $penjualan->commodity['code'] }}</td>
                                <td>{{ $penjualan->commodity['name'] }}</td>
                                <td>Rp. {{ number_format($penjualan->commodity['buy_price'], 0,',','.') }}</td>
                                <td>{{ $penjualan->qty }} pcs</td>
                                <td>Rp. {{ number_format($penjualan->sub_total, 0,',','.') }}</td>
                                <td>Rp. {{ number_format($penjualan->commodity['profit'], 0,',','.') }}</td>
                                <td>{{ $penjualan->transactions->created_at->format('d/m/Y H:i') }}</td>
                            </tr>
                            @php
                                $no++;
                            @endphp
                        @endforeach
                    @else
                    @endif
                </tbody>
                {{-- <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>SUM</td>
                        <td>PROFIT</td>
                        <td></td>
                    </tr>
                </tfoot> --}}
            </table>
        </div>
    </body>
</html>
