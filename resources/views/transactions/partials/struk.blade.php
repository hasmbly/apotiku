<!-- Modal -->
<div class="modal fade" id="struk-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title w-100 text-center">
                    {{-- <h5 id="title-struk-modal"><strong>Struk</strong></h5> --}}
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-sm-12 text-center">
                        <h5><strong>Apotek Mita Farma</strong></h5>
                        <div class="text-monospace">
                        Jl. Raya Paseh No. 223, Paseh Kidul,
                        <br />
                        Kec. Paseh, Kab. Sumedang 45381
                        </div>
                    </div>
                </div>
                <div class="row h-25 justify-content-left mt-4">
                    <div class="col-sm-2 text-left text-monospace">
                        Nomor
                    </div>
                    <div class="col-sm-1 text-center text-monospace">
                        :
                    </div>
                    <div class="col-sm-auto text-left text-monospace struk-code">
                    </div>
                </div>
                <div class="row h-25 justify-content-left">
                    <div class="col-sm-2 text-left text-monospace">
                        Tanggal
                    </div>
                    <div class="col-sm-1 text-center text-monospace">
                        :
                    </div>
                    <div class="col-sm-auto text-left text-monospace struk-date">
                    </div>
                </div>
                <div class="row h-25 justify-content-left">
                    <div class="col-sm-2 text-left text-monospace">
                        Operator
                    </div>
                    <div class="col-sm-1 text-center text-monospace">
                        :
                    </div>
                    <div class="col-sm-auto text-left text-monospace">
                        {{ auth()->user()->name }}
                    </div>
                </div>
                <hr class="text-monospace" style="border-top: dashed 1px;" />

                <div class="transaction-struk-items"></div>

                <hr class="text-monospace" style="border-top: dashed 1px;" />
                <div class="row h-25 justify-content-end">
                    <div class="col-sm-2 text-left text-monospace">
                        Total
                    </div>
                    <div class="col-sm-1 text-center text-monospace">
                        :
                    </div>
                    <div class="col-sm-auto text-left text-monospace struk-total">
                    </div>
                </div>
                <div class="row h-25 justify-content-end">
                    <div class="col-sm-2 text-left text-monospace">
                        Tunai
                    </div>
                    <div class="col-sm-1 text-center text-monospace">
                        :
                    </div>
                    <div class="col-sm-auto text-left text-monospace struk-tunai">
                    </div>
                </div>
                <hr class="text-monospace" style="border-top: dashed 1px;" />
                <div class="row h-25 justify-content-end">
                    <div class="col-sm-2 text-left text-monospace">
                        Kembali
                    </div>
                    <div class="col-sm-1 text-center text-monospace">
                        :
                    </div>
                    <div class="col-sm-auto text-left text-monospace struk-change">
                    </div>
                </div>
                <hr class="text-monospace" style="border-top: dashed 1px;" />
                <div class="row justify-content-center mt-3">
                    <div class="col-sm-12 text-center">
                        <div class="text-monospace">
                        Terima Kasih, Atas Kunjungan Anda
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" id="button-close-struk-modal" data-dismiss="modal">Selesai</button>

                {{-- <a href="{{ route('transactions.export.pdf') }}" class="btn btn-primary btn-sm" id="button-cetak-struk-modal" disabled>Cetak</a> --}}
                <button type="button" class="btn btn-primary btn-sm" id="button-cetak-struk-modal" disabled>Cetak</button>
            </div>
        </div>
    </div>
</div>
