@extends('layouts.app', ['title' => __('Laporan Penjualan')])

@section('content')
    @include('transactions.partials.header', ['title' => __('Laporan Penjualan')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            {{-- <div class="col-3">
                                <h3 class="mb-0">{{ __('Data Laporan Penjualan') }}</h3>
                            </div> --}}
                            <div class="col-sm-auto">

                                <form method="get" action="{{ route('transactions.report_penjualan') }}" autocomplete="off">
                                    {{-- @csrf --}}

                                            <div class="form-group">
                                                <div class="input-group input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                    </div>
                                                    <input class="form-control datepicker" name="start_date" placeholder="Pilih Tanggal" type="text" value="{{ old('start_date') }}" autofocus>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-info">{{ __('Cari') }}</button>

                                            {{-- <a href="{{ route('commodities.create') }}" class="btn btn-info">
                                                <i class="fas fa-search"></i> {{ __('Cari') }}
                                            </a> --}}
                                            <a href="{{ route('transactions.export.pdf') }}" class="btn btn-success" target="_blank">
                                                <i class="fas fa-print"></i> {{ __('Cetak') }}
                                            </a>
                                </form>
                            </div>
                                <div class="col-sm-8 text-right mt-2">
                                    <h3 class="mb-0 btn btn-sm btn-white">
                                        <span class="badge badge-pill badge-primary">Total</span> Rp. {{ number_format($total_penjualan, 0,',','.') }}
                                    </h3>

                                    <h3 class="mb-0 btn btn-sm btn-white">
                                        <span class="badge badge-pill badge-primary">Profit</span> Rp. {{ number_format($total_profit, 0,',','.') }}
                                    </h3>
                                </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">{{ __('Kode Barang') }}</th>
                                    <th scope="col">{{ __('Nama Barang') }}</th>
                                    <th scope="col">{{ __('Harga') }}</th>
                                    <th scope="col">{{ __('Jumlah') }}</th>
                                    <th scope="col">{{ __('Sub Total') }}</th>
                                    <th scope="col">{{ __('Profit') }}</th>
                                    <th scope="col">{{ __('Tanggal') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @if (!empty($penjualans))
                                    @foreach ($penjualans as $penjualan)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $penjualan->commodity->code }}</td>
                                            <td>{{ $penjualan->commodity->name }}</td>
                                            <td>Rp. {{ number_format($penjualan->commodity->buy_price, 0,',','.') }}</td>
                                            <td>{{ $penjualan->qty }} pcs</td>
                                            <td>Rp. {{ number_format($penjualan->sub_total, 0,',','.') }}</td>
                                            <td>Rp. {{ number_format($penjualan->commodity->profit, 0,',','.') }}</td>
                                            <td>{{ $penjualan->transactions->created_at->format('d/m/Y H:i') }}</td>
                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                @else
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-center" aria-label="...">
                            {{ $penjualans->appends($_GET)->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
@endpush
