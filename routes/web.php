<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function ()
{
    Route::group(['middleware' => ['role:ADMIN']], function ()
    {
        Route::group(['middleware' => ['role:SUPER_ADMIN']], function ()
        {
            Route::resource('user', 'UserController', ['except' => ['show']]);
        });

        Route::resource('commodityTypes', 'CommodityTypeController');
        Route::resource('bentukSediaan', 'BentukSediaanController');
        Route::resource('produsen', 'ProdusenController');
    });

    Route::resource('commodities', 'CommodityController', ['except' => ['show']]);
    Route::post('commodities/search_commodity', 'CommodityController@SearchData');
	Route::get('commodities/search', ['as' => 'commodities.search', 'uses' => 'CommodityController@search']);
	Route::get('commodities/getCommodityQtyByID', ['as' => 'commodities.getCommodityQtyByID', 'uses' => 'CommodityController@getCommodityQtyByID']);
	Route::get('commodities/export', ['as' => 'commodities.export', 'uses' => 'CommodityController@export']);
    Route::post('commodities/import', ['as' => 'commodities.import', 'uses' => 'CommodityController@import']);

    Route::resource('transactions', 'TransactionsController');
    Route::get('transactions/create/{type}', ['as' => 'transactions.create', 'uses' => 'TransactionsController@create']);
    Route::post('transactions/store/items', ['as' => 'transactions.store.items', 'uses' => 'TransactionsController@storeItems']);
	Route::get('transactions/export/pdf', ['as' => 'transactions.export.pdf', 'uses' => 'TransactionsController@exportPdf']);
	Route::get('transactions/report/penjualan', ['as' => 'transactions.report_penjualan', 'uses' => 'TransactionsController@reportPenjualan']);
	Route::get('transactions/report/penjualan/search_date', ['as' => 'transactions.report_penjualan.search_date', 'uses' => 'TransactionsController@reportPenjualanSearch']);

	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});
